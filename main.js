export function setup(ctx) {
    // debug
    const debugLog = (...msg) => {
        mod.api.SEMI.log(`${id} v4286`, ...msg);
    };

    // variables
    const id = "semi-auto-bury";
    const name = "SEMI Auto Bury";

    const itemList = game.items.bones.allObjects.sort((a,b) => a.sellsFor - b.sellsFor);

    let config = {
        items: [],
        enabled: false
    };

    const isItemSelected = (itemid) => config.items.indexOf(itemid) !== -1;
    const toggleItem = (itemid) => {
        let idx = config.items.indexOf(itemid);
        if (idx !== -1) {
            config.items.splice(idx, 1);
        }
        else {
            config.items.push(itemid);
        }
    };

    // modal
    const injectModal = () => {
        // Overlay Modal
        const scriptModal = mod.api.SEMI.buildModal(id, `${name}`);
        scriptModal.blockContainer.html(`
            <div class="block-content pt-0 font-size-sm">
                <div class="row semi-grid xs pt-1">
                    <div class="col-lg-6 col-md-12">
                        <div class="w-100">
                            <div class="custom-control custom-switch custom-control-lg">
                                <input class="custom-control-input" type="checkbox" name="${id}-enable-check" id="${id}-enable-check">
                                <label class="font-weight-normal ml-2 custom-control-label" for="${id}-enable-check">Enable ${name}</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="w-100 semi-grid-item">
                            Disabled
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="w-100 semi-grid-item enable">
                            Enabled
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-content pt-0 font-size-sm">
                <div class="pb-4">
                    <h2 class="content-heading border-bottom mb-2 pb-2">Item Toggles</h2>
                    <div class="row semi-grid"  id="${id}-container">

                    </div>
                </div>
            </div>
        `);

        $(scriptModal.modal).on('hidden.bs.modal', () => {
            clearContainer();
            storeConfig();

            if (config.enabled) {
                itemList.forEach(item => {
                    processItem(item);
                });
            }
        });

        $(`#${id}-enable-check`).on('change', function(e) {
            toggleEnabledStatus();
        });

        $(`#${id}-enable-check`).prop('checked', config.enabled);
    };

    const showModal = () => {
        updateModal();
        $(`#modal-${id}`).modal('show');
    };

    const updateModal = () => {
        clearContainer();
        buildItems();
    };

    const clearContainer = () => {
        $(`#${id}-container`).empty();
    };

    const toggleEnabledStatus = () => {
        config.enabled = !config.enabled;
        storeConfig();
    };

    const buildItems = () => {
        $(`#${id}-container`).html(itemList.map(buildItem).join(''));

        $(`#${id}-container .semi-grid-item`).on('click', function(e) {
            const elm = $(this);
            const itemid = elm.data("item");

            elm.toggleClass('enable');
            toggleItem(itemid);
        });
    }

    const buildItem = (item) => {
        const isSelected = isItemSelected(item.id);

        return `<div class="p-1 col-lg-4 col-md-6 col-sm-12">
        <div class="semi-grid-item ${isItemSelected(item.id) ? 'enable' : ''}" data-item="${item.id}">
            <img src="${item.media}" alt="${item.name}" /> ${item.name}
        </div>
    </div>`;
    }

    // script
    const processItem = (item) => {
        if (!config.enabled || !isItemSelected(item.id) || mod.api.SEMI.isBankFull() || !game.prayer.isUnlocked) {
            return;
        }

        let qty = game.bank.getQty(item);

        // All but One
        const allButOneIsEnabled = mod.api.SEMI.getSubmodSettings('SEMI All But One', 'all-but-one');
        if (allButOneIsEnabled) {
            qty--;
        }

        // Exit if there's nothing to open
        if (qty <= 0) {
            return;
        }

        // Open the items
        game.bank.buryItemOnClick(item, qty);

        // Send a notification to the user
        debugLog(`Buried ${qty} of ${item.name}`);
        mod.api.SEMI.customNotify(
            item.media,
            `Buried ${qty} of ${item.name}`,
            {lowPriority:true}
        );
    }

    // config
    const storeConfig = () => {
        ctx.characterStorage.setItem('config', config);
    }

    const loadConfig = () => {
        const storedConfig = ctx.characterStorage.getItem('config');
        if (!storedConfig) {
            return;
        }

        config = { ...config, ...storedConfig };
    }

    // hooks + game patches
    ctx.onCharacterLoaded(ctx => {
        loadConfig();

        // First do a pass on the players bank, opening any chests that have accumulated before the mod ran
        if (config.enabled) {
            itemList.forEach(item => {
                processItem(item);
            });
        }

        // Patches the add item to bank function, running this every time an object we care about is added to the bank
        ctx.patch(Bank, 'addItem').after((didAddItem, item, ...args) => {
            if (didAddItem) {
                processItem(item);
            }
        });
    });

    ctx.onInterfaceReady(() => {
        injectModal();
        mod.api.SEMI.addSideBarModSetting(name, ctx.getResourceUrl('semi_icon.png'), showModal);
    });
}
